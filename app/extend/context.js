module.exports = {
  // 成功时候的提示
  apiSuccess(data = '', msg = 'ok', code = 200) {
    this.body = { msg, data, code };
    this.status = code;
  },

  // 失败时候的提示
  apiFail(data = '', msg = 'fail', code = 201) {
    this.body = { msg, data, code };
    this.status = code;
  },

  // 验证token
  checkToken(token) {
	  return this.app.jwt.verify(token, this.app.config.jwt.secret);
  },
  
};