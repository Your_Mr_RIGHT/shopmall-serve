module.exports = {
  success(ctx,res) {
    ctx.status = res.status ? res.status : 200;
    // if (res.status) {
    //   delete res.status;
    // }
    ctx.body = {
      data: res.data ? res.data : null,
      code: res.code ? res.code : 0, // 0代表成功，其他代表失败
      msg: res.msg ? res.msg : "请求成功",
    };
  },
}