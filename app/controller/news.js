'use strict';

const Controller = require('egg').Controller;

class NewsController extends Controller {
  async list() {
    const { ctx, service } = this;
    const dataList = {
      list: [
        { id: 1, title: 'this is news 1', url: '/news/1' },
        { id: 2, title: 'this is news 2', url: '/news/2' },
      ],
    };
    const res = await service.news.list();
    ctx.body = res
    // await ctx.render('news/list.tpl', dataList);
  }

  async file(){
    const { ctx} = this;
    console.log(ctx.request.files);
    // const file = ctx.request.files[0];
    // console.log(file);
    // ctx.body = file
  }
}

module.exports = NewsController;
