'use strict';

const Controller = require('egg').Controller;

class AdminUserController extends Controller {
  // 注册
  async add() {
    const { ctx, service } = this;
    const body = ctx.request.body
    const res = await service.admin.adminUser.add(body);
    ctx.helper.success(ctx,res)
  }

  // 登录
  async login() {
    const { ctx, service } = this;
    const data = ctx.request.body
    const res = await service.admin.adminUser.login(data);
    // console.log(res);
    ctx.helper.success(ctx,res)
  }

  // 退出登录
  async logout() {
    const { ctx, service } = this;
    const res = await service.admin.adminUser.logout();
    ctx.helper.success(ctx,res)
  }
}

module.exports = AdminUserController;
