'use strict';

const { success } = require('../../extend/helper');

const Controller = require('egg').Controller;

class CategoryController extends Controller {
  // 大类
  async category() {
    const { ctx, service } = this;
    const data = ctx.request.body
    const res = await service.admin.category.category();
    ctx.helper.success(ctx,res)
  }

  // 小类
  async category_sub(){
    const { ctx, service } = this;
    const {categoryId} = ctx.request.body;
    console.log(categoryId);
    const res = await service.admin.category.category_sub(categoryId);
    ctx.helper.success(ctx,res)
  }

  // 根据小类获取分页商品
  async getGoodsListByCategorySubID(){
    const { ctx ,service} = this;
    const {categorySubID,page} =  ctx.request.body;
    const res = await service.admin.category.getGoodsListByCategorySubID(categorySubID,page);
    ctx.header,success(ctx,res)
  }

}

module.exports = CategoryController;
