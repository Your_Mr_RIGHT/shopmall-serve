'use strict';

const Controller = require('egg').Controller;

class GoodsController extends Controller {
  async goodDetail() {
    const { ctx, service } = this;
    const data = ctx.request.body
    const res = await service.admin.goods.goodDetail(data);
    console.log(res);
    ctx.helper.success(ctx,res)
  }
}

module.exports = GoodsController;
