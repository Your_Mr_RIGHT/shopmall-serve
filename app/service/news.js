const Service = require('egg').Service;

class NewsService extends Service {
  async list(page = 1) {
    // read config
    const { serverUrl, limit } = this.config.news;

    // use build-in http client to GET hacker-news api
    const res  = await this.ctx.curl(
      `${serverUrl}?$limit=${limit}`,
      {
        dataType: 'json',
      },
    );

    console.log("res:",res.data.data);
    return res.data.data;
  }
}

module.exports = NewsService;