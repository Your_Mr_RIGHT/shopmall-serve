const Service = require('egg').Service;

class GoodsService extends Service {
  async goodDetail(params) {
    const { ctx, app } = this;
    let { id } = params;

    // 判断当前商品是否存在
    const goods = await app.mysql.get('goods', {id})
    if (goods == null) {
       return  {
         code:-1,
         msg : "查无此商品!"
         
       }
    }
    return {
      data: {
        goods,
      },
      msg: "查询成功",
    };
  }
}

module.exports = GoodsService;