const Service = require('egg').Service;

class CategoryService extends Service {
  // 商品大类
  async category() {
    const { ctx, app } = this;
    const category = await app.mysql.select('category')
    return {
      data: {
        category,
      },
      msg: "查询成功",
    };
  }

  // 根据商品大类查询小类
  async category_sub(categoryId) {
    const { ctx, app } = this;
    console.log(categoryId);
    const category_sub = await app.mysql.select('category_sub', {
      where:{MALL_CATEGORY_ID: categoryId }
    })
    return {
      data: {
        category_sub,
      },
      msg: "查询成功",
    };
  }

  // 根据小类获取分页商品
  async getGoodsListByCategorySubID(categorySubID,page){
    const num =10;
    const {ctx,app} = this;
    const goods = await app.mysql.select('goods',{
      where :{SUB_ID:categorySubID},
      limit: page*num, // 返回数据量
      offset: (page-1)*num, // 数据偏移量
    })
    return {
      data:{
        goods
      },
      msg:"查询成功"
    }
  }

  
}

module.exports = CategoryService;