const Service = require('egg').Service;
const sha256 = require('js-sha256').sha256;
class AdminUserService extends Service {
  // 注册
  async add(body) {
    const { app } = this;
    let { userAccount, nickName, password, roleId } = body;

    // 判断当前账户是否存在
    const resSelect = await app.mysql.get('user', { userAccount })
    if (resSelect !== null) {
      return  {
        code:-1,
        msg : "注册失败 该账户已存在!"
      }
    }

    // 判断当前昵称是否存在
    const resSelect1 = await app.mysql.get('user', { nickName })
    if (resSelect1 !== null) {
      return  {
        code:-1,
        msg : "注册失败 该昵称已存在!"
      }
    }

    // 获取当前时间戳
    const created_at = (new Date()).getTime();
    // 执行数据库插入语句
    const res = await app.mysql.insert('user', { userAccount, nickName, password: sha256(password), roleId, created_at })
    console.log(res);
    if (res.affectedRows  === 1) {
      return {
        msg :"用户注册成功"
      }
    }else {
      return {
        code:-1,
        msg :"用户注册失败"
      }
    }
  }

  // 登录
  async login(params) {
    const { ctx, app } = this;
    let { userAccount, password } = params;

    // 判断当前账户是否存在
    const user = await app.mysql.get('user', { userAccount, password: sha256(password) })
    if (user == null) {
       return  {
         code:-1,
         msg : "账户或者密码错误!"
         
       }
    }
    // 加密token
    const token = app.jwt.sign({ user }, app.config.jwt.secret, { expiresIn: '24h' });
    user.token = token;
    // 设置cookies 
    ctx.cookies.set('token', token, {
      maxAge:86400000,
      httpOnly: false,
      signed: false,
    });
    const userName = user.nickName;
    return {
      data: {
        token,
        userName: userName,
      },
      msg: "登录成功",
    };
  }

  // 退出登录
  async logout(params) {
    const { ctx, app } = this;
    // 设置cookies 
    ctx.cookies.set('token', '', {
      maxAge:0,
    });
    return  {
      msg : "退出成功!"
    }
  }
}

module.exports = AdminUserService;