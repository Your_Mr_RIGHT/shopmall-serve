'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  const { router, controller } = app;
  router.get('/', controller.home.index);
  //-------------------------管理接口--------------------------
  router.post('/adminUser/add', controller.admin.adminUser.add);  //注册
  router.post('/adminUser/login', controller.admin.adminUser.login);  //登录
  router.post('/adminUser/logout', controller.admin.adminUser.logout);  //登录
  router.post('/goodDetail', controller.admin.goods.goodDetail);  //商品详情
  router.get('/category', controller.admin.category.category);  //商品大类接口
  router.post('/category_sub', controller.admin.category.category_sub);  //根据商品大类查询小类接口
  router.post('/getGoodsListByCategorySubID', controller.admin.category.getGoodsListByCategorySubID);  //根据商品大类查询小类接口

};
