'use strict';

/** @type Egg.EggPlugin */
module.exports = {

  // 跨域设置开启
  cors: {
    enable: true,
    package: 'egg-cors',
  },

  // 鉴权
  jwt: {
    enable: true,
    package: "egg-jwt"
  },

  // 模板语法开发
  nunjucks: {
    enable: true,
    package: 'egg-view-nunjucks',
  },

  // mysql支持
  mysql: {
    enable: true,
    package: 'egg-mysql',
  },

  // mysql  
  //   sequelize: {
  //     enable: true,
  //     package: 'egg-sequelize'
  //  }
};
