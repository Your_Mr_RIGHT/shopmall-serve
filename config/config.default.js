/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1648735209136_6849';


  // config.auth = {
	//   ignore: ['/adminUser/login','/auth'] // 指定数组中接口不需要进行token验证
	// };

  // 添加 view 配置 模板
  config.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
      '.tpl': 'nunjucks',
    },
  };

  // 关闭csrf开启跨域
  config.security = {
    // 关闭 csrf
    csrf: {
      enable: false,
    },
    // 跨域白名单
    domainWhiteList: ['*'],
  };

  // 允许跨域的方法
  config.cors = {
    origin: '*',
    allowMethods: 'GET, PUT, POST, DELETE, PATCH'
  };

  // jwt 鉴权
  config.jwt = {
    secret: "123456"
  };

  // 文件上传设置
  config.multipart = {
    mode: 'file',
  };

  // mysql数据库连接
  config.mysql = {
    // 单数据库信息配置
    client: {
      // host
      host: "121.5.109.97",
      // 端口号
      port: "3309",
      // 用户名
      user: "root",
      // 密码
      password: "",
      // 数据库名
      database: "bookdb",
    },
    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false,
  };



  // add your middleware config here
  config.middleware = ['auth'];
  config.auth = {
	  ignore: ['/adminUser/login','/auth'] // 指定数组中接口不需要进行token验证
	};
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
   
  };


  userConfig.news = {
    limit: 5,
    serverUrl: 'https://cnodejs.org/api/v1/topics',
  };
  
  return {
    ...config,
    ...userConfig,
  };
};
